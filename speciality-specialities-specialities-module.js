(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["speciality-specialities-specialities-module"],{

/***/ "./src/app/speciality/specialities/specialities.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/speciality/specialities/specialities.module.ts ***!
  \****************************************************************/
/*! exports provided: SpecialitiesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecialitiesPageModule", function() { return SpecialitiesPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _specialities_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./specialities.page */ "./src/app/speciality/specialities/specialities.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _specialities_page__WEBPACK_IMPORTED_MODULE_5__["SpecialitiesPage"]
    }
];
var SpecialitiesPageModule = /** @class */ (function () {
    function SpecialitiesPageModule() {
    }
    SpecialitiesPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
            ],
            declarations: [_specialities_page__WEBPACK_IMPORTED_MODULE_5__["SpecialitiesPage"]]
        })
    ], SpecialitiesPageModule);
    return SpecialitiesPageModule;
}());



/***/ }),

/***/ "./src/app/speciality/specialities/specialities.page.html":
/*!****************************************************************!*\
  !*** ./src/app/speciality/specialities/specialities.page.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n\n    <app-title></app-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div fxLayout=\"column\"\n       class=\"min-h-full\">\n    <div fxLayout=\"column\"\n         fxLayoutAlign=\"end\"\n         class=\"min-h-100vh bg-white\">\n      <div fxLayout=\"column\">\n       <!-- <h1 class=\"p-3 text-3xl text-center uppercase\">Consultorios San Roque</h1> -->\n        <h1 class=\"w-2/3 mr-auto p-3 text-2xl sm:text-3xl md:text-4xl text-grey-800 font-bold\">Todas las especialidades, brindadas por más de 100 profesionales prestadores</h1>\n\n        <h1 class=\"text-lg sm:text-2xl\" translate></h1>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>CLINICA MEDICA</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>CIRUGIA GENERAL Y VIDEOLAPAROSCOPICA</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>CIRUGIA CABEZA Y CUELLO</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>CIRUGIA MAXILO FACIAL</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>CARDIOLOGIA</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>CARDIOLOGIA INTERVENCIONISTA - HEMODINAMIA</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>CIRUGIA CARDIOVASCULAR, CENTRAL Y PERIFERICA</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>ELECTROFISIOLOGIA Y ABLACION POR RADIOFRECUENCIA MARCAPASOS</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>TRAUMATOLOGIA Y ORTOPEDIA</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>CIRUGIA ARTROSCOPICA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>GINECOLOGIA -Cir. Gin. Conv y VLP</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>GASTROENTEROLOGIA Y ENDOSCOPIA DIGESTIVA COLANGIOGRAFIA - CPRE</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>FLEBOLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>NEUROLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>NEUROLOGIA Y CIRUGIA ENDOVASCULAR</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>UROLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>TERAPIA INTENSIVA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>TERAPIA INTENSIVA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>UNIDAD CORONARIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>DIAGNOSTICO POR IMAGENES</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n           <ion-icon name=\"\"></ion-icon>\n            <span translate>| Resonancia Magnética</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n          <ion-icon name=\"\"></ion-icon>\n            <span translate>| Cardioresonancia</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"\"></ion-icon>\n            <span translate>| Tomografía Computada - Helicoidal 3D</span> \n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"\"></ion-icon>\n            <span translate>| CardioTAC</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"\"></ion-icon>\n            <span translate>| Ecografías General, Ginecológica y Urológica</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"\"></ion-icon>\n            <span translate>| Radiología Digital</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"\"></ion-icon>\n            <span translate>| Radiología Convencional</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"\"></ion-icon>\n            <span translate>| Manografía</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"\"></ion-icon>\n            <span translate>| Eco Doppler Color Cardiaco</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"\"></ion-icon>\n            <span translate>| Eco Doppler Color - Vascular Periférico</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>FISIOTERAPIA Y KINESIOLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>HEMATOLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>HEMOTERAPIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>INFECTOLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>ENDOCRINOLOGIA Y METABOLISMO</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>DIABETES Y NUTRICION</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>NUTRICION</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>NEFROLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>NEUMONOLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>MASTOLGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>ONCOLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>OFTALMOLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>OTORRINOLARINGOLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>FONOAUDIOLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>DERMATOLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>ALERGOLOGIA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>LABORATORIO - ANALISIS CLINICOS Y ALTA COMPLEJIDAD</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>ANATOMIA PATOLOGICA</span>\n          </div>\n\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>ANESTESIOLOGIA</span>\n          </div>\n\n          <h1 class=\"p-3 text-3xl text-center uppercase\">Consultorios San Roque</h1>  \n        <div fxLayout=\"column\" fxLayout.gt-xs=\"row\">\n          <div fxLayout=\"column\" fxLayoutAlign=\"center\">\n            <img src=\"assets/img/specialities/2.jpg\" class=\"w-4/5 sm:w-full h-auto mb-3 sm:mb-0 rounded-r\">\n          </div>\n\n          <div fxLayout=\"column\">\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\" fxLayoutGap=\"1rem\" class=\"z-20\">\n              <img src=\"assets/img/specialities/3.jpg\" fxFlex=\"33\" class=\"h-auto rounded\">\n              <img src=\"assets/img/specialities/4.jpg\" fxFlex=\"33\" class=\"h-auto rounded\">\n            </div>\n\n            <div fxLayout=\"row\" fxLayoutAlign=\"center center\" fxLayoutGap=\"1rem\" class=\"-mt-6\">\n              <img src=\"assets/img/specialities/1.jpg\" fxFlex=\"80\" class=\"h-auto rounded\">\n            </div>\n          </div>\n        </div>\n\n        <div fxLayout=\"column\"\n             fxLayoutAlign=\"center\"\n             fxFlex=\"100\" fxFlex.gt-xs=\"66\"\n             class=\"z-20 mb-4\">\n          <!--<h1 class=\"w-2/3 mr-auto p-3 text-2xl sm:text-3xl md:text-4xl text-grey-800 font-bold\">Todas las especialidades, brindadas por más de 100 profesionales prestadores</h1>-->\n        </div>\n      </div>\n    </div>\n\n    <div class=\"relative w-full bg-primary-600\">\n      <div fxLayout=\"row\" fxLayoutAlign=\"center\" class=\"-mt-4 md:mr-8\">\n        <div fxLayout=\"row wrap\"\n             fxLayoutGap=\"1rem grid\">\n          <div>\n            <div class=\"p-3 bg-white rounded shadow\">\n              <div fxLayout=\"column\" fxLayoutGap=\"1rem\" class=\"mb-3\">\n                <h1 class=\"text-lg\">Consultorios externos</h1>\n                <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n                  <ion-icon name=\"pin\" class=\"text-grey-700 text-2xl\"></ion-icon>\n                  <span class=\"text-grey-800 text-sm sm:text-lg\">Avda. Reyes Católicos 1.505</span>\n                </div>\n                <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\n                  <ion-icon name=\"call\" class=\"mr-3 text-grey-700 text-2xl\"></ion-icon>\n                  <a href=\"tel:+543874397582\" class=\"mr-2 px-2 py-1 bg-primary-50 text-sm sm:text-lg rounded-full no-underline\">4397582</a>\n                  <span class=\"mr-2\">-</span>\n                  <a href=\"tel:+543874397698\" class=\"px-2 py-1 bg-primary-50 text-sm sm:text-lg rounded-full no-underline\">4397698</a>\n                </div>\n              </div>\n\n              <div fxLayout=\"column\" fxLayoutGap=\"1rem\">\n                <h1 class=\"text-lg\">Consultorios San Patricio</h1>\n                <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n                  <ion-icon name=\"pin\" class=\"text-grey-700 text-2xl\"></ion-icon>\n                  <span class=\"text-grey-800 text-sm sm:text-lg\">Mitre 370</span>\n                </div>\n                <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\n                  <ion-icon name=\"call\" class=\"mr-3 text-grey-700 text-2xl\"></ion-icon>\n                  <a href=\"tel:+543874212033\" class=\"mr-2 px-2 py-1 bg-primary-50 text-sm sm:text-lg rounded-full no-underline\">4212033</a>\n                </div>\n              </div>\n            </div>\n          </div>\n\n<!--          <div>-->\n<!--            <div class=\"p-3 bg-white rounded shadow\">-->\n<!--              <h1 class=\"text-lg text-center\" translate>Quirófano</h1>-->\n<!--            </div>-->\n<!--          </div>-->\n\n<!--          <div>-->\n<!--            <div class=\"p-3 bg-white rounded shadow\">-->\n<!--              <h1 class=\"text-lg text-center\" translate>Cirugía General</h1>-->\n<!--            </div>-->\n<!--          </div>-->\n\n<!--          <div>-->\n<!--            <div class=\"p-3 bg-white rounded shadow\">-->\n<!--              <h1 class=\"text-lg text-center\" translate>Cirugía Traumatológica</h1>-->\n<!--            </div>-->\n<!--          </div>-->\n        </div>\n\n<!--        <div class=\"w-full md:w-4/5 xl:w-2/3 -mt-4 md:mr-8\">-->\n<!--          <ion-slides>-->\n<!--            <ion-slide *ngFor=\"let speciality of specialities\" class=\"h-full\">-->\n<!--              <div fxLayout=\"column\"-->\n<!--                   fxLayoutAlign=\"stretch\"-->\n<!--                   fxLayoutGap=\"0.5rem\"-->\n<!--                   class=\"w-48 md:w-64 h-full bg-white rounded shadow-lg\">-->\n<!--&lt;!&ndash;                <div [style.background-image]=\"'url(' + speciality?.picture + ')' | safe: 'style'\"&ndash;&gt;-->\n<!--&lt;!&ndash;                     class=\"w-full h-32 rounded-t bg-cover bg-center bg-no-repeat\"></div>&ndash;&gt;-->\n\n<!--                <h1 class=\"p-3 text-lg text-center\">{{speciality?.display}}</h1>-->\n<!--              </div>-->\n<!--            </ion-slide>-->\n<!--          </ion-slides>-->\n<!--        </div>-->\n      </div>\n    </div>\n\n    <app-footer></app-footer>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/speciality/specialities/specialities.page.scss":
/*!****************************************************************!*\
  !*** ./src/app/speciality/specialities/specialities.page.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NwZWNpYWxpdHkvc3BlY2lhbGl0aWVzL3NwZWNpYWxpdGllcy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/speciality/specialities/specialities.page.ts":
/*!**************************************************************!*\
  !*** ./src/app/speciality/specialities/specialities.page.ts ***!
  \**************************************************************/
/*! exports provided: SpecialitiesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecialitiesPage", function() { return SpecialitiesPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var ngx_take_until_destroy__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-take-until-destroy */ "./node_modules/ngx-take-until-destroy/fesm5/ngx-take-until-destroy.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SpecialitiesPage = /** @class */ (function () {
    function SpecialitiesPage(media) {
        this.media = media;
        this.specialities = [
            {
                display: 'Quirófano',
            },
            {
                display: 'Cirugía General',
            },
            {
                display: 'Cirugía Traumatológica',
            },
        ];
    }
    SpecialitiesPage.prototype.ngOnInit = function () {
        var _this = this;
        this.media
            .media$
            .pipe(Object(ngx_take_until_destroy__WEBPACK_IMPORTED_MODULE_3__["untilDestroyed"])(this))
            .subscribe(function () {
            if (_this.media.isActive('gt-xs')) {
                _this.slides.options = {
                    slidesPerView: 3,
                    autoplay: {
                        duration: 3000,
                        disableOnInteraction: false,
                    },
                };
            }
            else {
                _this.slides.options = {
                    slidesPerView: 1,
                    autoplay: {
                        duration: 3000,
                        disableOnInteraction: false,
                    },
                };
            }
            _this.slides.startAutoplay();
        });
    };
    SpecialitiesPage.prototype.ngOnDestroy = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonSlides"]),
        __metadata("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonSlides"])
    ], SpecialitiesPage.prototype, "slides", void 0);
    SpecialitiesPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-specialities',
            template: __webpack_require__(/*! ./specialities.page.html */ "./src/app/speciality/specialities/specialities.page.html"),
            styles: [__webpack_require__(/*! ./specialities.page.scss */ "./src/app/speciality/specialities/specialities.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["MediaObserver"]])
    ], SpecialitiesPage);
    return SpecialitiesPage;
}());



/***/ })

}]);
//# sourceMappingURL=speciality-specialities-specialities-module.js.map