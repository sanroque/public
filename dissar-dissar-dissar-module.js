(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dissar-dissar-dissar-module"],{

/***/ "./src/app/dissar/dissar/dissar.module.ts":
/*!************************************************!*\
  !*** ./src/app/dissar/dissar/dissar.module.ts ***!
  \************************************************/
/*! exports provided: DissarPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DissarPageModule", function() { return DissarPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _dissar_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dissar.page */ "./src/app/dissar/dissar/dissar.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _dissar_page__WEBPACK_IMPORTED_MODULE_5__["DissarPage"]
    }
];
var DissarPageModule = /** @class */ (function () {
    function DissarPageModule() {
    }
    DissarPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
            ],
            declarations: [_dissar_page__WEBPACK_IMPORTED_MODULE_5__["DissarPage"]]
        })
    ], DissarPageModule);
    return DissarPageModule;
}());



/***/ }),

/***/ "./src/app/dissar/dissar/dissar.page.html":
/*!************************************************!*\
  !*** ./src/app/dissar/dissar/dissar.page.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n\n    <app-title></app-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div fxLayout=\"column\"\n       class=\"min-h-full\">\n    <div fxLayout=\"column\"\n         fxLayoutGap=\"2rem\"\n         class=\"app-container mx-auto\">\n      <!--      <div *ngFor=\"let machine of machines\"-->\n      <!--           fxLayout=\"column\"-->\n      <!--           fxLayoutGap=\"1rem\">-->\n      <!--        <h1 class=\"text-lg text-2xl text-grey-900\">{{machine?.name}}</h1>-->\n\n      <!--        <div fxLayout=\"row wrap\"-->\n      <!--             fxLayoutAlign=\"center center\"-->\n      <!--             fxLayoutGap=\"1rem grid\">-->\n      <!--          <div *ngFor=\"let picture of machine?.pictures\"-->\n      <!--               fxLayout=\"column\"-->\n      <!--               class=\"w-64 h-64 rounded\">-->\n      <!--            <img [style.background-image]=\"'url(' + picture + ')' | safe:'style'\"-->\n      <!--                 class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\">-->\n      <!--          </div>-->\n      <!--        </div>-->\n      <!--      </div>-->\n\n\n      <div fxLayout=\"column\"\n           fxLayoutGap=\"1rem\">\n        <h1 class=\"text-2xl text-grey-900 text-center\">Resonador Magnético de Alto Campo, Superconductivo, Óptico de 1.5 Tesla - GE</h1>\n        <h1 class=\"text-lg text-grey-900 text-center\">Modelo Óptima MR 360 Advance de 16</h1>\n        <div fxLayout=\"row wrap\"\n             fxLayoutAlign=\"center stretch\"\n             fxLayoutGap=\"1rem grid\">\n          <div fxLayout=\"column\"\n               fxFlex=\"90\" fxFlex.gt-xs=\"40\"\n               class=\"h-86 rounded\">\n            <!--            <img src=\"assets/img/dissar/rm2.jpg\" class=\"w-full h-full rounded\">-->\n            <div [style.background-image]=\"'url(assets/img/dissar/rm2.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n          </div>\n          <div fxLayout=\"column\"\n               fxFlex=\"90\" fxFlex.gt-xs=\"40\"\n               class=\"h-86 rounded\">\n            <!--            <img src=\"assets/img/dissar/rm1.jpg\" class=\"w-full h-full rounded\">-->\n            <div [style.background-image]=\"'url(assets/img/dissar/rm1.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n          </div>\n        </div>\n      </div>\n\n      <div fxLayout=\"column\"\n           fxLayoutGap=\"1rem\">\n        <h1 class=\"text-2xl text-grey-900 text-center\">Radiología Digital</h1>\n        <div fxLayout=\"row wrap\"\n             fxLayoutAlign=\"center stretch\"\n             fxLayoutGap=\"1rem grid\">\n          <div fxLayout=\"column\"\n               fxFlex=\"90\" fxFlex.gt-xs=\"40\"\n               class=\"h-86 rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/014.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n            <!--            <img [style.background-image]=\"'url(assets/img/dissar/014.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\">-->\n          </div>\n        </div>\n      </div>\n\n      <div fxLayout=\"column\"\n           fxLayoutGap=\"1rem\">\n        <h1 class=\"text-2xl text-grey-900 text-center\">Ecógrafo Siemens Acuson NX3</h1>\n        <div fxLayout=\"row wrap\"\n             fxLayoutAlign=\"center stretch\"\n             fxLayoutGap=\"1rem grid\">\n          <div fxLayout=\"column\"\n               fxFlex=\"90\" fxFlex.gt-xs=\"33\"\n               style=\"height:23rem;\"\n               class=\"rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/005.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n          </div>\n          <div fxLayout=\"column\"\n               fxFlex=\"90\" fxFlex.gt-xs=\"33\"\n               style=\"height:23rem;\"\n               class=\"rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/006.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n          </div>\n          <div fxLayout=\"column\"\n               fxFlex=\"90\" fxFlex.gt-xs=\"33\"\n               style=\"height:23rem;\"\n               class=\"rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/007.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n          </div>\n        </div>\n      </div>\n\n      <div fxLayout=\"column\"\n           fxLayoutGap=\"1rem\">\n        <h1 class=\"text-2xl text-grey-900 text-center\">Ecógrafo Siemens Acuson X700</h1>\n        <div fxLayout=\"row wrap\"\n             fxLayoutAlign=\"center center\"\n             fxLayoutGap=\"1rem grid\">\n          <div fxLayout=\"column\"\n               fxFlex=\"90\" fxFlex.gt-xs=\"33\"\n               style=\"height:28rem;\"\n               class=\"rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/008.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n          </div>\n          <div fxLayout=\"column\"\n               fxFlex=\"90\" fxFlex.gt-xs=\"33\"\n               style=\"height:18rem;\"\n               class=\"rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/010.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n          </div>\n          <div fxLayout=\"column\"\n               fxFlex=\"90\" fxFlex.gt-xs=\"33\"\n               style=\"height:28rem;\"\n               class=\"rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/009.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n          </div>\n        </div>\n      </div>\n\n      <div fxLayout=\"column\"\n           fxLayoutGap=\"1rem\">\n        <h1 class=\"text-2xl text-grey-900 text-center\">Angiógrafo - GE INNOVA IGS530</h1>\n        <p class=\"text-lg text-grey-900 text-center\">· Apto para procedimientos hemodinamicos cardiológicos, neurológicos y esplácnicos (diagnósticos y terapéuticos)</p>\n        <p class=\"text-lg text-grey-900 text-center\">· Tecnologia de punta que aporta mayor seguridad al paciente y significativa reducción en días de internación</p>\n        <p class=\"text-lg text-grey-900 text-center\">· Mayor velocidad y calidad de imagen, con minima exposición a radiación</p>\n        <p class=\"text-lg text-grey-900 text-center\">· Reconstrucciones en 3d que optimizan el tratamiento mínimo invasivo de aneurismas y malformaciones arteriovenosas neurológicas</p>\n        <div fxLayout=\"row wrap\"\n             fxLayoutAlign=\"center center\"\n             fxLayoutGap=\"1rem grid\">\n          <div fxLayout=\"column\"\n               fxFlex=\"90\" fxFlex.gt-xs=\"50\"\n               style=\"height:28rem;\"\n               class=\"rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/012.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n          </div>\n          <div fxLayout=\"column\"\n               fxFlex=\"90\" fxFlex.gt-xs=\"50\"\n               style=\"height:28rem;\"\n               class=\"rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/angiografo.jpg)' | safe:'style'\" class=\"w-full h-full bg-cover bg-right bg-no-repeat rounded\"></div>\n          </div>\n        </div>\n      </div>\n\n      <div fxLayout=\"column\"\n           fxLayoutGap=\"1rem\">\n        <h1 class=\"text-2xl text-grey-900 text-center\">Tomografía Cardíaca - Coronariografía No Invasiva</h1>\n\n        <p>\n          En el marco del programa de mejora continua del Sanatorio San Roque  - DISSAR, se incorporo al Dr. Mariano Estofan, Cardiólogo Especialista formado en Fundación\n          Favaloro, Máster en Tomografía y Resonancia Cardíaca - Hospital Saint Paul de Barcelona y Director de Diplomatura  Multi-Imágenes Cardíacas de San Miguel de Tucumán;\n          quien conjuntamente con Cardiólogos especialistas de nuestro staff Dr. Sergio Argiró y Dra. Victoria Barroso  y el equipo de  licenciados especializados en Bio-Imágenes,\n          realizaron por primera vez en nuestra provincia, estudios de <strong>“Tomografía Cardíaca”</strong>.\n        </p>\n\n        <div fxLayout=\"row wrap\">\n          <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"60\">\n            <p>\n              La Cardio TC es uno de los métodos no invasivos más modernos para  el  diagnóstico\n              de las enfermedades cardiovasculares. Se realiza con Equipamiento de última generación:\n              Tomógrafo Multislice 64 cortes GE  REVOLUTION EVO.\n            </p>\n\n            <h1 class=\"p-2 text-lg font-bold text-center\">Fuimos pioneros en Resonancia Cardíaca en el 2016</h1>\n            <h1 class=\"p-2 text-lg font-bold text-center\">y ahora nuevamente en Tomografía Cardiaca</h1>\n\n            <div fxLayout=\"row wrap\"\n                 fxLayoutAlign=\"center center\"\n                 fxLayoutGap=\"1rem grid\">\n              <div fxLayout=\"column\"\n                   fxFlex=\"90\" fxFlex.gt-xs=\"66\"\n                   class=\"rounded\">\n                <div [style.background-image]=\"'url(assets/img/dissar/t/1.png)' | safe:'style'\"\n                     style=\"height: 20rem;\"\n                     class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n                <h1 class=\"p-2 text-lg text-center\">Resonador Magnético 1.5 TESLA - GE OPTIMA MR360</h1>\n              </div>\n              <div fxLayout=\"column\"\n                   fxFlex=\"90\" fxFlex.gt-xs=\"33\"\n                   class=\"rounded\">\n                <div [style.background-image]=\"'url(assets/img/dissar/t/c.jpg)' | safe:'style'\"\n                     style=\"height: 16rem;\"\n                     class=\"w-full h-full bg-contain bg-center bg-no-repeat rounded\"></div>\n                <h1 class=\"text-lg text-center\">Coronariografia</h1>\n              </div>\n            </div>\n          </div>\n\n          <div fxLayout=\"column\" fxFlex=\"80\" fxFlex.gt-sm=\"40\" class=\"mx-auto p-3\">\n            <div fxLayout=\"column\"\n                 class=\"rounded\">\n              <div [style.background-image]=\"'url(assets/img/dissar/t/2.jpg)' | safe:'style'\"\n                   style=\"height: 30rem;\"\n                   class=\"w-full h-full bg-cover bg-center bg-no-repeat rounded\"></div>\n              <h1 class=\"p-2 text-lg text-center\">Tomógrafo MULTISLICE 64 CORTES - GE REVOLUTION EVO</h1>\n            </div>\n          </div>\n        </div>\n      </div>\n\n\n\n      <div fxLayout=\"column\"\n           fxLayoutGap=\"1rem\">\n        <h1 class=\"text-lg text-2xl text-grey-900\">Tomografía Cardíaca</h1>\n        <div fxLayout=\"row wrap\"\n             fxLayoutAlign=\"center center\"\n             fxLayoutGap=\"1rem grid\">\n<!--          <img src=\"assets/img/dissar/t/c1.png\" fxFlex=\"100\" fxFlex.gt-xs=\"33\">-->\n<!--          <img src=\"assets/img/dissar/t/c2.png\" fxFlex=\"100\" fxFlex.gt-xs=\"30\">-->\n<!--          <img src=\"assets/img/dissar/t/c3.png\" fxFlex=\"100\" fxFlex.gt-xs=\"30\">-->\n          <div fxLayout=\"column\" style=\"height: 20rem;\" class=\"w-64 rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/t/c1.png)' | safe:'style'\" class=\"w-full h-full bg-black bg-contain bg-center bg-no-repeat rounded\"></div>\n          </div>\n          <div fxLayout=\"column\" style=\"height: 20rem;\" class=\"w-64 rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/t/c2.png)' | safe:'style'\" class=\"w-full h-full bg-black bg-contain bg-center bg-no-repeat rounded\"></div>\n          </div>\n          <div fxLayout=\"column\" style=\"height: 20rem;\" class=\"w-64 rounded\">\n            <div [style.background-image]=\"'url(assets/img/dissar/t/c3.png)' | safe:'style'\" class=\"w-full h-full bg-black bg-contain bg-center bg-no-repeat rounded\"></div>\n          </div>\n        </div>\n      </div>\n\n      <div fxLayout=\"column\"\n           fxLayoutGap=\"1rem\">\n        <h1 class=\"text-lg text-2xl text-grey-900\">Nódulos Pulmonares</h1>\n        <div fxLayout=\"row wrap\"\n             fxLayoutAlign=\"center center\"\n             fxLayoutGap=\"1rem grid\">\n          <img src=\"assets/img/dissar/t/np1.png\" fxFlex=\"100\" fxFlex.gt-xs=\"40\">\n          <img src=\"assets/img/dissar/t/np2.png\" fxFlex=\"100\" fxFlex.gt-xs=\"30\">\n          <img src=\"assets/img/dissar/t/np3.png\" fxFlex=\"100\" fxFlex.gt-xs=\"30\">\n<!--          <div fxLayout=\"column\" style=\"height: 20rem;\" class=\"w-64 rounded\">-->\n<!--            <div [style.background-image]=\"'url(assets/img/dissar/t/np1.png)' | safe:'style'\" class=\"w-full h-full bg-black bg-contain bg-center bg-no-repeat rounded\"></div>-->\n<!--          </div>-->\n<!--          <div fxLayout=\"column\" style=\"height: 20rem;\" class=\"w-64 rounded\">-->\n<!--            <div [style.background-image]=\"'url(assets/img/dissar/t/np2.png)' | safe:'style'\" class=\"w-full h-full bg-black bg-contain bg-center bg-no-repeat rounded\"></div>-->\n<!--          </div>-->\n<!--          <div fxLayout=\"column\" style=\"height: 20rem;\" class=\"w-64 rounded\">-->\n<!--            <div [style.background-image]=\"'url(assets/img/dissar/t/np3.png)' | safe:'style'\" class=\"w-full h-full bg-black bg-contain bg-center bg-no-repeat rounded\"></div>-->\n<!--          </div>-->\n        </div>\n      </div>\n    </div>\n\n    <app-footer></app-footer>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/dissar/dissar/dissar.page.scss":
/*!************************************************!*\
  !*** ./src/app/dissar/dissar/dissar.page.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2Rpc3Nhci9kaXNzYXIvZGlzc2FyLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/dissar/dissar/dissar.page.ts":
/*!**********************************************!*\
  !*** ./src/app/dissar/dissar/dissar.page.ts ***!
  \**********************************************/
/*! exports provided: DissarPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DissarPage", function() { return DissarPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DissarPage = /** @class */ (function () {
    function DissarPage() {
        this.machines = [
        // {
        //   name: 'Resonador Magnético 1.5 TESLA - GE',
        //   pictures: [
        //     'assets/img/dissar/001.jpg',
        //     'assets/img/dissar/002.jpg',
        //     'assets/img/dissar/003.jpg',
        //     'assets/img/dissar/004.jpg',
        //   ],
        // },
        // {
        //   name: 'Radiología Digital',
        //   pictures: [
        //     'assets/img/dissar/014.jpg',
        //   ],
        // },
        // {
        //   name: 'Ecógrafo Siemens Acuson NX3',
        //   pictures: [
        //     'assets/img/dissar/005.jpg',
        //     'assets/img/dissar/006.jpg',
        //     'assets/img/dissar/007.jpg',
        //   ],
        // },
        // {
        //   name: 'Ecógrafo Siemens Acuson X700',
        //   pictures: [
        //     'assets/img/dissar/008.jpg',
        //     'assets/img/dissar/009.jpg',
        //     'assets/img/dissar/010.jpg',
        //   ],
        // },
        // {
        //   name: 'Angiógrafo INNOVA IGS530',
        //   pictures: [
        //     'assets/img/dissar/011.jpg',
        //     'assets/img/dissar/012.jpg',
        //     'assets/img/dissar/013.jpg',
        //   ],
        // },
        // {
        //   name: 'Tomógrafo MULTISLICE 64 CORTES - REVOLUTION EVO',
        //   pictures: [
        //     'assets/img/dissar/t000.jpg',
        //     'assets/img/dissar/t001.jpg',
        //     'assets/img/dissar/t002.jpg',
        //     'assets/img/dissar/t003.jpg',
        //     'assets/img/dissar/t004.jpg',
        //   ],
        // },
        // {
        //   name: 'Tomografías Cardíacas',
        //   pictures: [
        //     'assets/img/dissar/tomcar1.png',
        //     'assets/img/dissar/tomcar2.png',
        //     'assets/img/dissar/tomcar3.png',
        //   ],
        // },
        ];
    }
    DissarPage.prototype.ngOnInit = function () {
    };
    DissarPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dissar',
            template: __webpack_require__(/*! ./dissar.page.html */ "./src/app/dissar/dissar/dissar.page.html"),
            styles: [__webpack_require__(/*! ./dissar.page.scss */ "./src/app/dissar/dissar/dissar.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DissarPage);
    return DissarPage;
}());



/***/ })

}]);
//# sourceMappingURL=dissar-dissar-dissar-module.js.map