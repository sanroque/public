(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["internment-internment-internment-module"],{

/***/ "./src/app/internment/internment/internment.module.ts":
/*!************************************************************!*\
  !*** ./src/app/internment/internment/internment.module.ts ***!
  \************************************************************/
/*! exports provided: InternmentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InternmentPageModule", function() { return InternmentPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _internment_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./internment.page */ "./src/app/internment/internment/internment.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _internment_page__WEBPACK_IMPORTED_MODULE_5__["InternmentPage"]
    }
];
var InternmentPageModule = /** @class */ (function () {
    function InternmentPageModule() {
    }
    InternmentPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
            ],
            declarations: [_internment_page__WEBPACK_IMPORTED_MODULE_5__["InternmentPage"]]
        })
    ], InternmentPageModule);
    return InternmentPageModule;
}());



/***/ }),

/***/ "./src/app/internment/internment/internment.page.html":
/*!************************************************************!*\
  !*** ./src/app/internment/internment/internment.page.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\n  <ion-header>\n    <ion-toolbar>\n      <ion-buttons slot=\"start\">\n        <ion-back-button defaultHref=\"/\"></ion-back-button>\n      </ion-buttons>\n\n      <app-title></app-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <div fxLayout=\"column\"\n       class=\"app-container mx-auto\">\n    <div fxLayout=\"row wrap\">\n      <div fxLayout=\"column\"\n           fxFlex=\"100\" fxFlex.gt-xs=\"50\">\n        <p class=\"my-3 sm:my-8 text-lg sm:text-3xl text-grey-800 text-center z-50\"\n           translate>Nuestras instalaciones le garantizan privacidad, aislamiento acústico, amplias dimensiones, luz natural y vistas panorámicas, elementos aliados de la curación del paciente.</p>\n\n        <div fxLayout=\"column\"\n             fxLayoutAlign=\"center\"\n             fxFlex=\"1 1 auto\"\n             class=\"-mr-8\">\n          <div style=\"background-image: url('assets/img/dots-small.svg');\"\n               class=\"h-32 w-full mt-0 sm:-mt-8 -mb-8\"></div>\n        </div>\n      </div>\n\n      <div fxLayout=\"column\"\n           fxFlex=\"100\" fxFlex.gt-xs=\"50\">\n        <img src=\"assets/img/internment/000.jpg\"\n             class=\"w-2/3 h-auto ml-auto rounded z-30\">\n\n        <img src=\"assets/img/internment/001.jpg\"\n             class=\"w-3/4 h-auto -mt-4 mr-auto ml-0 sm:-ml-8 rounded z-20\">\n\n        <img src=\"assets/img/internment/002.jpg\"\n             class=\"w-1/2 h-auto -mt-3 ml-auto rounded z-10\">\n      </div>\n\n       <div fxLayout=\"column\"\n           fxFlex=\"100\" fxFlex.gt-xs=\"50\">\n         <div fxLayout=\"column\"\n              fxLayoutGap=\"1rem\"\n              class=\"rooms-card p-4 bg-white text-grey-800 rounded shadow-tw-lg\">\n           <h1 class=\"text-lg sm:text-2xl\" translate>3 categorías de habitaciones:</h1>\n\n           <h1 class=\"text-lg sm:text-2xl\" translate>Standard - Superiores - Suites</h1>\n\n           <h1 class=\"text-lg sm:text-2xl\" translate>Suites Inteligentes:</h1>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>Sala de espera exclusiva</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>Servicios de Enfermería y Mucama exclusivos</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>Menú diferenciado para el paciente y acompañante</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>Cama Stryker eléctrica</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>Vistas panorámicas</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>Cómodas dimensiones</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>TV LCD</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>Aire acondicionado</span>\n           </div>\n\n           <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\">\n             <ion-icon name=\"checkmark\"></ion-icon>\n             <span translate>Frigo Bar y Amenities</span>\n           </div>\n         </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"relative mt-4 min-h-90vh\">\n    <ion-slides class=\"absolute pin w-full h-90vh\">\n      <ion-slide class=\"w-full h-90vh\">\n        <div [style.background-image]=\"'url(assets/img/suites/1.jpg)' | safe: 'style'\"\n             class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-90vh\">\n        <div [style.background-image]=\"'url(assets/img/suites/2.jpg)' | safe: 'style'\"\n             class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-90vh\">\n        <div [style.background-image]=\"'url(assets/img/suites/3.jpg)' | safe: 'style'\"\n             class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-90vh\">\n        <div [style.background-image]=\"'url(assets/img/suites/4.jpg)' | safe: 'style'\"\n             class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-90vh\">\n        <div [style.background-image]=\"'url(assets/img/suites/5.jpg)' | safe: 'style'\"\n             class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-90vh\">\n        <div [style.background-image]=\"'url(assets/img/suites/6.jpg)' | safe: 'style'\"\n             class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n    </ion-slides>\n\n    <div fxLayout=\"column\"\n         class=\"absolute pin app-container h-full mx-auto z-10\">\n      <h1 style=\"margin-top: -2.5rem;\"\n          class=\"mr-auto px-3 py-2 bg-white text-3xl md:text-4xl text-secondary-600 border-b-2 border-secondary rounded\" translate>Suites Inteligentes</h1>\n    </div>\n  </div>\n\n\n  <div class=\"relative mt-4 min-h-90vh\">\n    <ion-slides class=\"absolute pin w-full h-90vh\">\n      <ion-slide class=\"w-full h-90vh\">\n        <div fxLayout=\"column\"\n             fxLayoutAlign=\"start start\"\n             fxLayoutGap=\"1rem\"\n             class=\"p-4 text-grey-800 z-30\">\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\" class=\"suites-card mr-auto p-2 rounded z-30\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>Circulación semi restringida</span>\n          </div>\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\" class=\"suites-card mr-auto p-2 rounded z-30\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>Superficie de 550 m2 + Sala de Espera</span>\n          </div>\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\" class=\"suites-card mr-auto p-2 rounded z-30\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>Habitaciones individuales de 20 m2, baños privados y vistas panorámicas</span>\n          </div>\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\" class=\"suites-card mr-auto p-2 rounded z-30\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>16 Camas Stryker - Elementos de confort</span>\n          </div>\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\" class=\"suites-card mr-auto p-2 rounded z-30\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>Avanzada Tecnología Médica</span>\n          </div>\n        </div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-90vh\">\n        <div\n          [style.background-image]=\"'url(assets/img/uti/1.jpg)' | safe: 'style'\"\n          class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-90vh\">\n        <div\n          [style.background-image]=\"'url(assets/img/uti/2.jpg)' | safe: 'style'\"\n          class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-90vh\">\n        <div\n          [style.background-image]=\"'url(assets/img/uti/3.jpg)' | safe: 'style'\"\n          class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n    </ion-slides>\n\n    <div fxLayout=\"column\"\n         class=\"absolute pin app-container h-full mx-auto z-10\">\n      <h1 style=\"margin-top: -2.5rem;\"\n          class=\"mr-auto px-3 py-2 bg-white text-3xl md:text-4xl text-secondary-600 border-b-2 border-secondary rounded\"\n          translate>Internación en UTI</h1>\n    </div>\n  </div>\n\n  <div class=\"relative mt-4 min-h-90vh\">\n    <ion-slides class=\"absolute pin w-full h-90vh\">\n      <ion-slide class=\"w-full h-90vh\">\n        <div fxLayout=\"column\"\n             fxLayoutAlign=\"start start\"\n             fxLayoutGap=\"1rem\"\n             class=\"p-4 text-grey-800 z-30\">\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\" class=\"suites-card mr-auto p-2 rounded z-30\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>Circulación semi restringida</span>\n          </div>\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\" class=\"suites-card mr-auto p-2 rounded z-30\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>Superficie de 390 m2 + Sala de Espera</span>\n          </div>\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\" class=\"suites-card mr-auto p-2 rounded z-30\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>Habitaciones individuales, baños privados y vistas panorámicas</span>\n          </div>\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\" class=\"suites-card mr-auto p-2 rounded z-30\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>Camas Stryker - Elementos de confort</span>\n          </div>\n          <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"1rem\" class=\"suites-card mr-auto p-2 rounded z-30\">\n            <ion-icon name=\"checkmark\"></ion-icon>\n            <span translate>Avanzada Tecnología Médica</span>\n          </div>\n        </div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-90vh\">\n        <div\n          [style.background-image]=\"'url(assets/img/uco/1.jpg)' | safe: 'style'\"\n          class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-90vh\">\n        <div\n          [style.background-image]=\"'url(assets/img/uco/2.jpg)' | safe: 'style'\"\n          class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-90vh\">\n        <div\n          [style.background-image]=\"'url(assets/img/uco/3.jpg)' | safe: 'style'\"\n          class=\"w-full h-90vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n    </ion-slides>\n\n    <div fxLayout=\"column\"\n         class=\"absolute pin app-container h-full mx-auto z-10\">\n      <h1 style=\"margin-top: -2.5rem;\"\n          class=\"mr-auto px-3 py-2 bg-white text-3xl md:text-4xl text-secondary-600 border-b-2 border-secondary rounded\"\n          translate>Internación en UCO</h1>\n    </div>\n  </div>\n\n  <app-footer class=\"block bg-indigo-600 pt-4\"></app-footer>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/internment/internment/internment.page.scss":
/*!************************************************************!*\
  !*** ./src/app/internment/internment/internment.page.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".rooms-card {\n  margin-top: -1rem;\n  margin-right: 1rem; }\n  @media only screen and (min-width: 600px) {\n    .rooms-card {\n      margin-top: -66%;\n      margin-right: 0; } }\n  .suites-card {\n  background-color: rgba(255, 255, 255, 0.8); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3NhbnRpYWdvL3NhbnJvcXVlL2Zyb250ZW5kL3NyYy9hcHAvaW50ZXJubWVudC9pbnRlcm5tZW50L2ludGVybm1lbnQucGFnZS5zY3NzIiwiL2hvbWUvc2FudGlhZ28vc2Fucm9xdWUvZnJvbnRlbmQvc3JjL3N0eWxlcy91dGlscy9icmVha3BvaW50cy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGtCQUFrQixFQUFBO0VDWWxCO0lEZEY7TUFJSSxnQkFBZ0I7TUFDaEIsZUFBZSxFQUFBLEVBRWxCO0VBRUQ7RUFDRSwwQ0FBMkIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2ludGVybm1lbnQvaW50ZXJubWVudC9pbnRlcm5tZW50LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi9zdHlsZXMvdXRpbHMvYnJlYWtwb2ludHNcIjtcblxuLnJvb21zLWNhcmQge1xuICBtYXJnaW4tdG9wOiAtMXJlbTtcbiAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICBAaW5jbHVkZSBicmVha3BvaW50KCRndC14cykge1xuICAgIG1hcmdpbi10b3A6IC02NiU7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xuICB9XG59XG5cbi5zdWl0ZXMtY2FyZCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoI2ZmZiwgMC44KTtcbn0iLCIkeHM6ICdzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDU5OXB4KSc7XG4kc206ICdzY3JlZW4gYW5kIChtaW4td2lkdGg6IDYwMHB4KSBhbmQgKG1heC13aWR0aDogOTU5cHgpJztcbiRtZDogJ3NjcmVlbiBhbmQgKG1pbi13aWR0aDogOTYwcHgpIGFuZCAobWF4LXdpZHRoOiAxMjc5cHgpJztcbiRsZzogJ3NjcmVlbiBhbmQgKG1pbi13aWR0aDogMTI4MHB4KSBhbmQgKG1heC13aWR0aDogMTkxOXB4KSc7XG4keGw6ICdzY3JlZW4gYW5kIChtaW4td2lkdGg6IDE5MjBweCkgYW5kIChtYXgtd2lkdGg6IDUwMDBweCknO1xuXG4kbHQtc206ICdzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDU5OXB4KSc7XG4kbHQtbWQ6ICdzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk1OXB4KSc7XG4kbHQtbGc6ICdzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyNzlweCknO1xuJGx0LXhsOiAnc2NyZWVuIGFuZCAobWF4LXdpZHRoOiAxOTE5cHgpJztcbiRndC14czogJ3NjcmVlbiBhbmQgKG1pbi13aWR0aDogNjAwcHgpJztcbiRndC1zbTogJ3NjcmVlbiBhbmQgKG1pbi13aWR0aDogOTYwcHgpJztcbiRndC1tZDogJ3NjcmVlbiBhbmQgKG1pbi13aWR0aDogMTI4MHB4KSc7XG4kZ3QtbGc6ICdzY3JlZW4gYW5kIChtaW4td2lkdGg6IDE5MjBweCknO1xuXG5AbWl4aW4gYnJlYWtwb2ludCgkbWVkaWEpIHtcbiAgQG1lZGlhIG9ubHkgI3skbWVkaWF9IHtcbiAgICBAY29udGVudDtcbiAgfVxufVxuIl19 */"

/***/ }),

/***/ "./src/app/internment/internment/internment.page.ts":
/*!**********************************************************!*\
  !*** ./src/app/internment/internment/internment.page.ts ***!
  \**********************************************************/
/*! exports provided: InternmentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InternmentPage", function() { return InternmentPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InternmentPage = /** @class */ (function () {
    function InternmentPage() {
    }
    InternmentPage.prototype.ngOnInit = function () {
    };
    InternmentPage.prototype.ngAfterViewInit = function () {
        this.slides.forEach(function (slides) {
            slides.options = {
                autoplay: {
                    duration: 3000,
                    disableOnInteraction: false,
                },
            };
            slides.startAutoplay();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChildren"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonSlides"]),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["QueryList"])
    ], InternmentPage.prototype, "slides", void 0);
    InternmentPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-internment',
            template: __webpack_require__(/*! ./internment.page.html */ "./src/app/internment/internment/internment.page.html"),
            styles: [__webpack_require__(/*! ./internment.page.scss */ "./src/app/internment/internment/internment.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], InternmentPage);
    return InternmentPage;
}());



/***/ })

}]);
//# sourceMappingURL=internment-internment-internment-module.js.map