(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["guest-guests-guests-module"],{

/***/ "./src/app/guest/guests/guests.module.ts":
/*!***********************************************!*\
  !*** ./src/app/guest/guests/guests.module.ts ***!
  \***********************************************/
/*! exports provided: GuestsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuestsPageModule", function() { return GuestsPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _guests_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./guests.page */ "./src/app/guest/guests/guests.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _guests_page__WEBPACK_IMPORTED_MODULE_5__["GuestsPage"]
    }
];
var GuestsPageModule = /** @class */ (function () {
    function GuestsPageModule() {
    }
    GuestsPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
            ],
            declarations: [_guests_page__WEBPACK_IMPORTED_MODULE_5__["GuestsPage"]]
        })
    ], GuestsPageModule);
    return GuestsPageModule;
}());



/***/ }),

/***/ "./src/app/guest/guests/guests.page.html":
/*!***********************************************!*\
  !*** ./src/app/guest/guests/guests.page.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n\n    <app-title></app-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div fxLayout=\"row\"\n       fxLayoutAlign=\"center\"\n       class=\"app-container mx-auto\">\n    <div fxLayout=\"column\"\n         fxLayoutGap=\"2rem\"\n         class=\"w-full sm:w-2/3 mx-auto md:my-4 p-3 sm:p-4 border rounded\">\n      <h1 class=\"text-2xl text-primary font-bold\">Preparacion Previa</h1>\n\n      <p>Se necesita un recipiente estéril (puede ser adquirido en farmacias)</p>\n      <p>Se recolecta la primera orina de la mañana o  a cualquier hora del día con tres horas de retención</p>\n      <p>Mantener el recipiente con la muestra en un lugar fresco.</p>\n      <p>Rotular el recipiente con nombre , apellido  y  la fecha de nacimiento del paciente.</p>\n      <p>Entregar la muestra al laboratorio el mismo día de la recolección.</p>\n\n      <h1 class=\"text-2xl text-primary font-bold\">Tipo de Muestra</h1>\n      <p>Orina</p>\n\n      <h1 class=\"text-2xl text-primary font-bold\">Resultados</h1>\n      <p>24 hs.</p>\n    </div>\n  </div>\n\n  <app-footer></app-footer>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/guest/guests/guests.page.scss":
/*!***********************************************!*\
  !*** ./src/app/guest/guests/guests.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2d1ZXN0L2d1ZXN0cy9ndWVzdHMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/guest/guests/guests.page.ts":
/*!*********************************************!*\
  !*** ./src/app/guest/guests/guests.page.ts ***!
  \*********************************************/
/*! exports provided: GuestsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuestsPage", function() { return GuestsPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GuestsPage = /** @class */ (function () {
    function GuestsPage() {
    }
    GuestsPage.prototype.ngOnInit = function () {
    };
    GuestsPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-guests',
            template: __webpack_require__(/*! ./guests.page.html */ "./src/app/guest/guests/guests.page.html"),
            styles: [__webpack_require__(/*! ./guests.page.scss */ "./src/app/guest/guests/guests.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], GuestsPage);
    return GuestsPage;
}());



/***/ })

}]);
//# sourceMappingURL=guest-guests-guests-module.js.map