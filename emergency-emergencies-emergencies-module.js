(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["emergency-emergencies-emergencies-module"],{

/***/ "./src/app/emergency/emergencies/emergencies.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/emergency/emergencies/emergencies.module.ts ***!
  \*************************************************************/
/*! exports provided: EmergenciesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmergenciesPageModule", function() { return EmergenciesPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _emergencies_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./emergencies.page */ "./src/app/emergency/emergencies/emergencies.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _emergencies_page__WEBPACK_IMPORTED_MODULE_5__["EmergenciesPage"]
    }
];
var EmergenciesPageModule = /** @class */ (function () {
    function EmergenciesPageModule() {
    }
    EmergenciesPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
            ],
            declarations: [_emergencies_page__WEBPACK_IMPORTED_MODULE_5__["EmergenciesPage"]]
        })
    ], EmergenciesPageModule);
    return EmergenciesPageModule;
}());



/***/ }),

/***/ "./src/app/emergency/emergencies/emergencies.page.html":
/*!*************************************************************!*\
  !*** ./src/app/emergency/emergencies/emergencies.page.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n\n    <app-title></app-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div fxLayout=\"column\"\n       class=\"relative min-h-full\">\n    <div style=\"background-image: url(assets/img/emergencies/bg.jpg)\" class=\"absolute pin w-full h-full bg-cover bg-bottom bg-no-repeat\"></div>\n\n    <div fxLayout=\"column\"\n         fxLayoutAlign=\"center center\"\n         fxLayoutGap=\"2rem\"\n         fxFlex\n         class=\"z-10\">\n      <h1 class=\"text-3xl sm:text-5xl text-white text-center font-bold text-shadow\" translate>Ingreso por Las Palmeras 51</h1>\n      <h1 class=\"text-2xl sm:text-4xl text-white text-center font-bold text-shadow\" translate>Atención médica las 24hs</h1>\n      <a href=\"tel:03874390202\" matRipple\n         class=\"mr-2 p-3 bg-secondary-500 text-lg text-white rounded-full no-underline\">\n        <div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"0.5rem\">\n          <ion-icon name=\"call\" color=\"light\"></ion-icon>\n          <span class=\"font-bold\">0387 439 4000</span>\n        </div>\n      </a>\n    </div>\n  </div>\n\n  <div class=\"bg-indigo-600\">\n    <div fxLayout=\"column\" fxLayout.gt-xs=\"row\"\n         fxLayoutAlign=\"center center\"\n         fxLayoutGap=\"2rem\"\n         class=\"app-container mx-auto -mt-8\">\n      <div fxLayout=\"column\"\n           fxFlex=\"50\">\n        <img src=\"assets/img/emergencies/1.jpg\" class=\"w-full h-auto rounded shadow-tw-lg z-10\">\n      </div>\n\n      <div fxLayout=\"column\"\n           fxFlex=\"50\"\n           class=\"relative\">\n        <img src=\"assets/img/emergencies/2.jpg\" class=\"w-full h-auto rounded shadow-tw-lg z-10\">\n<!--        <div class=\"absolute pin-r pin-b w-full h-full p-3 z-20\">-->\n          <h1 class=\"absolute pin-r pin-b p-3 text-2xl sm:text-4xl text-center z-20\" translate>Shock Room</h1>\n<!--        </div>-->\n      </div>\n    </div>\n\n    <app-footer></app-footer>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/emergency/emergencies/emergencies.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/emergency/emergencies/emergencies.page.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtZXJnZW5jeS9lbWVyZ2VuY2llcy9lbWVyZ2VuY2llcy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/emergency/emergencies/emergencies.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/emergency/emergencies/emergencies.page.ts ***!
  \***********************************************************/
/*! exports provided: EmergenciesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmergenciesPage", function() { return EmergenciesPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var EmergenciesPage = /** @class */ (function () {
    function EmergenciesPage() {
    }
    EmergenciesPage.prototype.ngOnInit = function () {
    };
    EmergenciesPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-emergencies',
            template: __webpack_require__(/*! ./emergencies.page.html */ "./src/app/emergency/emergencies/emergencies.page.html"),
            styles: [__webpack_require__(/*! ./emergencies.page.scss */ "./src/app/emergency/emergencies/emergencies.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], EmergenciesPage);
    return EmergenciesPage;
}());



/***/ })

}]);
//# sourceMappingURL=emergency-emergencies-emergencies-module.js.map