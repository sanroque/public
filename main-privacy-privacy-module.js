(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main-privacy-privacy-module"],{

/***/ "./src/app/main/privacy/privacy.module.ts":
/*!************************************************!*\
  !*** ./src/app/main/privacy/privacy.module.ts ***!
  \************************************************/
/*! exports provided: PrivacyPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivacyPageModule", function() { return PrivacyPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _privacy_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./privacy.page */ "./src/app/main/privacy/privacy.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _privacy_page__WEBPACK_IMPORTED_MODULE_5__["PrivacyPage"]
    }
];
var PrivacyPageModule = /** @class */ (function () {
    function PrivacyPageModule() {
    }
    PrivacyPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
            ],
            declarations: [_privacy_page__WEBPACK_IMPORTED_MODULE_5__["PrivacyPage"]]
        })
    ], PrivacyPageModule);
    return PrivacyPageModule;
}());



/***/ }),

/***/ "./src/app/main/privacy/privacy.page.html":
/*!************************************************!*\
  !*** ./src/app/main/privacy/privacy.page.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\n    </ion-buttons>\n\n    <app-title></app-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div fxLayout=\"column\"\n       class=\"bg-white min-h-full\">\n    <div class=\"app-container mx-auto my-4 leading-loose rounded border\">\n      <h1>Cookie Policy for Sanatorio San Roque 2019</h1>\n      <p>This is the Cookie Policy for Sanatorio San Roque 2019, accessible from https://sanatoriosanroquesa.com.ar</p>\n      <p><strong>What Are Cookies</strong></p>\n      <p>As is common practice with almost all professional websites this site uses cookies, which are tiny files that are downloaded to your computer, to improve your experience. This page describes what information they gather, how we use it and why we sometimes need to store these cookies. We will also share how you can prevent these cookies from being stored however this may downgrade or 'break' certain elements of the sites functionality.</p>\n      <p>For more general information on cookies see the Wikipedia article on HTTP Cookies.</p>\n      <p><strong>How We Use Cookies</strong></p>\n      <p>We use cookies for a variety of reasons detailed below. Unfortunately in most cases there are no industry standard options for disabling cookies without completely disabling the functionality and features they add to this site. It is recommended that you leave on all cookies if you are not sure whether you need them or not in case they are used to provide a service that you use.</p>\n      <p><strong>Disabling Cookies</strong></p>\n      <p>You can prevent the setting of cookies by adjusting the settings on your browser (see your browser Help for how to do this). Be aware that disabling cookies will affect the functionality of this and many other websites that you visit. Disabling cookies will usually result in also disabling certain functionality and features of the this site. Therefore it is recommended that you do not disable cookies.</p>\n      <p><strong>The Cookies We Set</strong></p>\n      <ul>\n        <li>\n          <p>Email newsletters related cookies</p>\n          <p>This site offers newsletter or email subscription services and cookies may be used to remember if you are already registered and whether to show certain notifications which might only be valid to subscribed/unsubscribed users.</p>\n        </li>\n        <li>\n          <p>Forms related cookies</p>\n          <p>When you submit data to through a form such as those found on contact pages or comment forms cookies may be set to remember your user details for future correspondence.</p>\n        </li>\n      </ul>\n      <p><strong>Third Party Cookies</strong></p>\n      <p>In some special cases we also use cookies provided by trusted third parties. The following section details which third party cookies you might encounter through this site.</p>\n      <ul>\n        <li>\n          <p>This site uses Google Analytics which is one of the most widespread and trusted analytics solution on the web for helping us to understand how you use the site and ways that we can improve your experience. These cookies may track things such as how long you spend on the site and the pages that you visit so we can continue to produce engaging content.</p>\n          <p>For more information on Google Analytics cookies, see the official Google Analytics page.</p>\n        </li>\n        <li>\n          <p>We also use social media buttons and/or plugins on this site that allow you to connect with your social network in various ways. For these to work the following social media sites including; Facebook, Instagram, Twitter, will set cookies through our site which may be used to enhance your profile on their site or contribute to the data they hold for various purposes outlined in their respective privacy policies.</p>\n        </li>\n      </ul>\n      <p><strong>More Information</strong></p>\n      <p>Hopefully that has clarified things for you and as was previously mentioned if there is something that you aren't sure whether you need or not it's usually safer to leave cookies enabled in case it does interact with one of the features you use on our site. This Cookies Policy was created with the help of the <a href=\"https://cookiepolicygenerator.com\">GDPR Cookies Policy Generator</a></p>\n      <p>However if you are still looking for more information then you can contact us through one of our preferred contact methods:</p>\n      <ul>\n        <li>Email: info@sanatoriosanroquesa.com.ar</li>\n      </ul>\n    </div>\n\n    <span fxFlex></span>\n\n    <app-footer></app-footer>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/main/privacy/privacy.page.scss":
/*!************************************************!*\
  !*** ./src/app/main/privacy/privacy.page.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21haW4vcHJpdmFjeS9wcml2YWN5LnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/main/privacy/privacy.page.ts":
/*!**********************************************!*\
  !*** ./src/app/main/privacy/privacy.page.ts ***!
  \**********************************************/
/*! exports provided: PrivacyPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivacyPage", function() { return PrivacyPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PrivacyPage = /** @class */ (function () {
    function PrivacyPage() {
    }
    PrivacyPage.prototype.ngOnInit = function () {
    };
    PrivacyPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-privacy-page',
            template: __webpack_require__(/*! ./privacy.page.html */ "./src/app/main/privacy/privacy.page.html"),
            styles: [__webpack_require__(/*! ./privacy.page.scss */ "./src/app/main/privacy/privacy.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PrivacyPage);
    return PrivacyPage;
}());



/***/ })

}]);
//# sourceMappingURL=main-privacy-privacy-module.js.map