(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["external-office-external-offices-external-offices-module"],{

/***/ "./src/app/external-office/external-offices/external-offices.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/external-office/external-offices/external-offices.module.ts ***!
  \*****************************************************************************/
/*! exports provided: ExternalOfficesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExternalOfficesPageModule", function() { return ExternalOfficesPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _external_offices_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./external-offices.page */ "./src/app/external-office/external-offices/external-offices.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _external_offices_page__WEBPACK_IMPORTED_MODULE_5__["ExternalOfficesPage"]
    }
];
var ExternalOfficesPageModule = /** @class */ (function () {
    function ExternalOfficesPageModule() {
    }
    ExternalOfficesPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
            ],
            declarations: [_external_offices_page__WEBPACK_IMPORTED_MODULE_5__["ExternalOfficesPage"]]
        })
    ], ExternalOfficesPageModule);
    return ExternalOfficesPageModule;
}());



/***/ }),

/***/ "./src/app/external-office/external-offices/external-offices.page.html":
/*!*****************************************************************************!*\
  !*** ./src/app/external-office/external-offices/external-offices.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n\n    <ion-title translate>Consultorios externos</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div fxLayout=\"column\"\n       class=\"min-h-full\"></div>\n\n  <app-footer></app-footer>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/external-office/external-offices/external-offices.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/external-office/external-offices/external-offices.page.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2V4dGVybmFsLW9mZmljZS9leHRlcm5hbC1vZmZpY2VzL2V4dGVybmFsLW9mZmljZXMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/external-office/external-offices/external-offices.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/external-office/external-offices/external-offices.page.ts ***!
  \***************************************************************************/
/*! exports provided: ExternalOfficesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExternalOfficesPage", function() { return ExternalOfficesPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ExternalOfficesPage = /** @class */ (function () {
    function ExternalOfficesPage() {
    }
    ExternalOfficesPage.prototype.ngOnInit = function () {
    };
    ExternalOfficesPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-external-offices',
            template: __webpack_require__(/*! ./external-offices.page.html */ "./src/app/external-office/external-offices/external-offices.page.html"),
            styles: [__webpack_require__(/*! ./external-offices.page.scss */ "./src/app/external-office/external-offices/external-offices.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ExternalOfficesPage);
    return ExternalOfficesPage;
}());



/***/ })

}]);
//# sourceMappingURL=external-office-external-offices-external-offices-module.js.map