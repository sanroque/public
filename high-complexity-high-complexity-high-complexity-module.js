(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["high-complexity-high-complexity-high-complexity-module"],{

/***/ "./src/app/high-complexity/high-complexity/high-complexity.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/high-complexity/high-complexity/high-complexity.module.ts ***!
  \***************************************************************************/
/*! exports provided: HighComplexityPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HighComplexityPageModule", function() { return HighComplexityPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _high_complexity_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./high-complexity.page */ "./src/app/high-complexity/high-complexity/high-complexity.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _high_complexity_page__WEBPACK_IMPORTED_MODULE_5__["HighComplexityPage"]
    }
];
var HighComplexityPageModule = /** @class */ (function () {
    function HighComplexityPageModule() {
    }
    HighComplexityPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
            ],
            declarations: [_high_complexity_page__WEBPACK_IMPORTED_MODULE_5__["HighComplexityPage"]]
        })
    ], HighComplexityPageModule);
    return HighComplexityPageModule;
}());



/***/ }),

/***/ "./src/app/high-complexity/high-complexity/high-complexity.page.html":
/*!***************************************************************************!*\
  !*** ./src/app/high-complexity/high-complexity/high-complexity.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n\n    <app-title></app-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div fxLayout=\"column\"\n       class=\"relative min-h-full\">\n    <ion-slides class=\"w-full min-h-100vh\">\n      <ion-slide class=\"w-full min-h-100vh\">\n        <div fxLayout=\"column\" fxLayoutAlign=\"center center\" fxLayoutGap=\"2rem\" class=\"p-3\">\n          <ion-list lines=\"full\"\n                    class=\"min-w-1/3 border\">\n            <ion-list-header class=\"text-2xl text-secondary font-bold\">Alta Complejidad</ion-list-header>\n            <ion-item><ion-label class=\"text-center\">Unidad Terapia Intensiva de Alta Complejidad - UTI</ion-label></ion-item>\n            <ion-item><ion-label class=\"text-center\">Unidad Coronaria de Alta Complejidad - UCO</ion-label></ion-item>\n            <ion-item><ion-label class=\"text-center\">Cirugía Videolaparoscopica</ion-label></ion-item>\n            <ion-item><ion-label class=\"text-center\">Cirugía Cardiovascular y periférica</ion-label></ion-item>\n            <ion-item><ion-label class=\"text-center\">Hemodinamia diagnóstica y terapéutica</ion-label></ion-item>\n            <ion-item><ion-label class=\"text-center\">Neurocirugía convencional y endovascular</ion-label></ion-item>\n            <ion-item><ion-label class=\"text-center\">Tomógrafo Multislice 64 cortes - GE REVOLUTION EVO</ion-label></ion-item>\n            <ion-item><ion-label class=\"text-center\">Resonador Magnético 1.5 Tesla - GE OPTIMA MR360</ion-label></ion-item>\n            <ion-item><ion-label class=\"text-center\">Angiografo - GE INNOVA IGS530</ion-label></ion-item>\n          </ion-list>\n        </div>\n      </ion-slide>\n      <ion-slide class=\"w-full h-full min-h-100vh\">\n        <div [style.background-image]=\"'url(assets/img/high-complexity/bg.jpg)' | safe: 'style'\"\n             class=\"w-full h-full min-h-100vh bg-cover bg-center bg-no-repeat\"></div>\n      </ion-slide>\n    </ion-slides>\n  </div>\n\n  <app-footer></app-footer>\n\n  <!--<div fxLayout=\"column\"-->\n       <!--class=\"app-container mx-auto\">-->\n\n    <!--• CIRUGÍA CARDIOVASCULAR, CENTRAL Y PERIFÉRICA-->\n    <!--HEMODINAMIA Y CARDIOLOGÍA INTERVENCIONISTA. ANGIOGRAFO GE IGS 530, UNICO EN SALTA-->\n    <!--NEUROCIRUGÍA CONVENCIONAL Y ENDOVASCULAR-->\n  <!--</div>-->\n</ion-content>\n"

/***/ }),

/***/ "./src/app/high-complexity/high-complexity/high-complexity.page.scss":
/*!***************************************************************************!*\
  !*** ./src/app/high-complexity/high-complexity/high-complexity.page.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-list {\n  background-color: rgba(255, 255, 255, 0.8); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL3NhbnRpYWdvL3NhbnJvcXVlL2Zyb250ZW5kL3NyYy9hcHAvaGlnaC1jb21wbGV4aXR5L2hpZ2gtY29tcGxleGl0eS9oaWdoLWNvbXBsZXhpdHkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsMENBQTJCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9oaWdoLWNvbXBsZXhpdHkvaGlnaC1jb21wbGV4aXR5L2hpZ2gtY29tcGxleGl0eS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tbGlzdCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoI2ZmZiwgMC44KTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/high-complexity/high-complexity/high-complexity.page.ts":
/*!*************************************************************************!*\
  !*** ./src/app/high-complexity/high-complexity/high-complexity.page.ts ***!
  \*************************************************************************/
/*! exports provided: HighComplexityPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HighComplexityPage", function() { return HighComplexityPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HighComplexityPage = /** @class */ (function () {
    function HighComplexityPage() {
    }
    HighComplexityPage.prototype.ngOnInit = function () {
        this.slides.options = {
            autoHeight: true,
            autoplay: {
                duration: 3000,
                disableOnInteraction: false,
            },
        };
        this.slides.startAutoplay();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonSlides"]),
        __metadata("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonSlides"])
    ], HighComplexityPage.prototype, "slides", void 0);
    HighComplexityPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-high-complexity',
            template: __webpack_require__(/*! ./high-complexity.page.html */ "./src/app/high-complexity/high-complexity/high-complexity.page.html"),
            styles: [__webpack_require__(/*! ./high-complexity.page.scss */ "./src/app/high-complexity/high-complexity/high-complexity.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HighComplexityPage);
    return HighComplexityPage;
}());



/***/ })

}]);
//# sourceMappingURL=high-complexity-high-complexity-high-complexity-module.js.map